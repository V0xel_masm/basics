#include <stdio.h>

void mile_na_km(float *mile, float *kilometry, unsigned int n);

int main()
{
	float mile_morskie[4] = { 422.3, 971.2, 240.12, 5385.2 };
	float km[4];
	
	mile_na_km(mile_morskie, km, 4);
	for (int i = 0; i < 4; i++) printf("\n %f %f\n", mile_morskie[i], km[i]);
	return 0;
}
