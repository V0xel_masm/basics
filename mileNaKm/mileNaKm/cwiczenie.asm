.686
.model flat
public _mile_na_km
.code
_mile_na_km PROC

push EBP
mov EBP, ESP
push ebx
  
mov ECX, [EBP+16] ; liczba element�w tablicy
mov ebx, [ebp+8] ;adres tablica mil
mov edx, [ebp+12] ;adres tablica km
finit

push dword PTR 1852 
fild dword PTR [esp]
add esp, 4
push dword PTR 1000 ;dzielnik
fild dword PTR [esp]
add esp,4

zamiana:
fld dword PTR [ebx] ;wrzucenie mili
fmul st(0), st(2) ;razy 1852
fdiv st(0), st(1) ;dzielimy przez 1000
fstp dword PTR [edx] ;wrzucenie do km
add ebx, 4
add edx, 4
loop zamiana

pop edx
pop EBP
ret 	; powrot do programu glownego

_mile_na_km ENDP


END