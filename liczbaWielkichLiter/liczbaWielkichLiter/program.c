#include <stdio.h>

int liczba_wielkich_liter(char wiersz[]);

int main()
{
	char tekst[128]; int wynik;
	printf("\nProsze napisac dowolny tekst \
w jednym wierszu: ");
	//scanf_s("%s", tekst); blednie w zadaniu
	scanf_s("%[^\n]s", tekst, 128);
	wynik = liczba_wielkich_liter(tekst);
	printf("\nW podanym tekscie znajduje \
sie %d wielkich liter\n", wynik);
	return 0;
}