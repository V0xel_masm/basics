.686
.model flat

public _liczba_wielkich_liter

.code
_liczba_wielkich_liter PROC
push ebp
mov ebp,esp
mov ecx, [ebp+8]
mov eax, 0 ;inicjalizacja eax do 0 -- licznik 
poczatek:
cmp byte PTR [ecx], 0 
je koniec ;gdy null
cmp byte PTR [ecx], 65
jl dalej ;gdy mniejsza
cmp byte PTR [ecx], 90
jg dalej ;gdy wieksza
inc eax
dalej:
inc ecx
jmp poczatek
koniec:						
pop ebp
ret
_liczba_wielkich_liter ENDP
END