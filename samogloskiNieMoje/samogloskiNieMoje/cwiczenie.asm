.686
.model flat 


public _podziel_wyraz

.data 
litery db 'aeiouy0'
liczba dd 0


       

.code

_podziel_wyraz PROC
	push EBP
	mov EBP,ESP				; Standard syntax


	mov EBX, [EBP+8]
	mov EDX, [EBP+12]
	mov ECX, 0

SprawdzLitere:
	mov AL, [EBX]				; Ustawiaj AL na aktualny znak
	mov ECX , 0					; Zeruj ECX za kazdym powrotem
			
	cmp AL, 0					; Jesli koniec wyrazu, przejdz do koniec
	je PreKoniec

	cmp liczba, 0
	je SprawdzTablice

	cmp liczba, 1
	je SprawdzOdwrotnie		

	jnz SprawdzLitere

SprawdzTablice:
	mov AH, [litery+ECX]

	cmp AL, AH
	je Zamien					; Jesli istnieje litera z tablicy zamien

	cmp AH, 0
	je Podnies					; Je�li koniec tablicy samoglosek, podnies EBX i przejdz dalej

	add ECX, 1					; Jesli nic, zwieksz index
	jnz SprawdzTablice

SprawdzOdwrotnie:
	mov AH, [litery+ECX]

	cmp AL, AH
	je Podnies				

	cmp AH, 0
	je Zamien					

	add ECX, 1					; Jesli nic, zwieksz index
	jnz SprawdzOdwrotnie

Podnies:
	mov [EDX], AL
	add EBX, 1					; Przesun sie na nastepna litere
	add EDX, 1
	jmp SprawdzLitere			; Wroc do petli

Zamien:
	mov AL, '#'					; Wrzuc do AL hash
	mov [EDX], AL				; Wrzuc do EBX zmieniona wartosc

	add EDX, 1
	add EBX, 1					; Zwieksz wartosc
	jmp SprawdzLitere


PreKoniec:
	mov AL, 0
	mov [EDX], AL


	cmp liczba, 1
	je Koniec

	mov EBX, 0
	mov EBX, [EBP+8]

	mov EDX, [EBP+16]
	inc liczba

	jmp SprawdzLitere

Koniec:

	mov AL, 0
	mov [EDX], AL
	mov ESP,EBP				 ; Standard syntax
	pop EBP
	ret
_podziel_wyraz ENDP
END