#include <stdio.h>
void zamiana_kropek_na_sredniki(char wiersz[]);
int main()
{
	char znaki[128];
	int odp;
	printf("\n Prosze napisac dowolny tekst w jednym wierszu:\n");
	odp = scanf_s("%[^\n]s", znaki, 128);
	if (odp)
	{
		zamiana_kropek_na_sredniki(znaki);
		printf("\Tekst po zmianie ma postac %s\n", znaki);
	}
	else printf("\nNie wpisano tekstu\n");
	return 0;
}