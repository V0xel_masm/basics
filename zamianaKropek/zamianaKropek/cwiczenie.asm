.686
.model flat

public _zamiana_kropek_na_sredniki

.code
_zamiana_kropek_na_sredniki PROC
push ebp
mov ebp,esp
push ebx
mov ebx, [ebp+8] ;adres tablicy do ebx
poczatek:
cmp byte PTR [ebx], 0 ;sprawdzamy czy koniec - null
je koniec
mov ecx, [ebx]
cmp byte PTR [ebx], 46
jne dalej ;nie bylo kropki
mov byte PTR [ebx], 59 ;zamieniamy kropke na srednik  
dalej:
inc ebx
jmp poczatek
koniec: 
pop ebx
pop ebp
ret
_zamiana_kropek_na_sredniki ENDP
END