#include <stdio.h>
void tabl_modyf(float tabl[], float n);

int main()
{
	float liczby[5] = { -10.3, 14.1, 28.2, -1.14, 0.003 };

	tabl_modyf(liczby, 5.0);
	for (int i = 0; i < 5; i++)
		printf("\n i=%d %f", i, liczby[i]);
	printf("\n");
	return 0;

}