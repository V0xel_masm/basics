.686
.model flat

public _tabl_modyf

.code
_tabl_modyf PROC
push ebp
mov ebp,esp
push ebx
finit
fld dword PTR[ebp+12] ;rozmiar tablicy float
push dword PTR 0	;tu laduje wynik ;alokuje obszar na esp taki trick
fisttp dword PTR [esp] ;konwersja na int z ucieciem i zaladowanie do [esp] tego obszaru co wyzej
mov ecx, [esp]	;laduje wynik do ecx ;dword PTr niepotrzebny bo do rejestru 4bajtowego i tak wrzuci 4bajty wtedy
add esp,4
mov ebx, [ebp+8] ;adres tabl do ebx
push dword PTR 3
fild dword PTR [esp] ;wrzucenie trojki
add esp, 4
mnozenie:
fld dword PTR [ebx]
fmul st(0), st(1) ;trojka w st(1)
fst dword PTR [ebx]
fstp st(0)
add ebx, 4 ;przejscie do kolejnego elementu
loop mnozenie
pop ebx			
pop ebp
ret
_tabl_modyf ENDP
END