.686
.model flat

public _suma

.code
_suma PROC ;wyznacza sume 3 arg
push ebp
mov ebp,esp
mov eax, [ebp+8]
add eax, [ebp+12]
add eax, [ebp+16]
add eax, [ebp+20]
pop ebp
ret
_suma ENDP

END