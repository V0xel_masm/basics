.686
.model flat

public _czest_rezonansowa

.code
_czest_rezonansowa PROC
push ebp
mov ebp,esp
push dword PTR 2
fild dword PTR [esp]
add esp, 4
fldpi ; pi w st0, st1-2
fmulp st(1), st(0) ; 2pi w st0
fld dword PTR [ebp+8] ;
fld dword PTR [ebp+12] ;st0-C st1-L , st2 - 2PI
fmulp st(1), st(0) ; st0 LC, st1 2pi
fsqrt ;st0 pierwLC
fmulp st(1), st(0) ; st0 2pipierw(LC)
fld1 ;st0 1, st1 2pipierw(LC)
fdiv st(0), st(1) ;st0 calosc			
pop ebp
ret
_czest_rezonansowa ENDP
END