.686
.model flat

public _najwieksza

.code
_najwieksza PROC
push ebp
mov ebp,esp
push ebx
finit
fld dword PTR[ebp+12] ;rozmiar tablicy float
push dword PTR 0	;tu laduje wynik ;alokuje obszar na esp taki trick
fisttp dword PTR [esp] ;konwersja na int z ucieciem i zaladowanie do [esp] tego obszaru co wyzej
mov ecx, dword PTR [esp]	;laduje wynik do ecx
add esp, 4
mov ebx, [ebp+8] ;adres do ebx
fld dword PTR [ebx] ;wrzucenie pierwszej
sprawdzanie:
fld dword PTR [ebx] ;st0 kolejna wartosc - w pierwszym obiegu porowna sie samo ze soba
fcomi st(0), st(1)
jb mniejsza
fstp st(1) ;st(0) do st(1) i pop wierzcholka -- najwieksza w st(0)
jmp dalej
mniejsza:
fstp st(0) ;usuwamy wierzcholek bo w st(1) byla wieksza
dalej:
add ebx, 4 ;przejscie na kolejny element 
loop sprawdzanie
pop ebx			
pop ebp
ret
_najwieksza ENDP
END