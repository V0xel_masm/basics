.686
.model flat

public _oblicz_pi

.code
_oblicz_pi PROC
push ebp
mov ebp,esp
push ebx
finit

mov ecx, [ebp+8] ;licznik n obiegow
mov edx, 0 ;pseudo bool
fld1 ;wrzucenie licznika
push dword PTR 3

sumowanie:
fld1
fild dword PTR [esp]
fdivp st(1), st(0)

cmp edx, 1
je dodaj
fsubp st(1), st(0)
mov edx, 1
jmp cos
dodaj:
faddp st(1), st(0)
mov edx, 0
cos:
add [esp], dword PTR 2
loop sumowanie

mov [esp], dword PTR 4
fild dword PTR [esp]
fmulp st(1), st(0)

add esp, 4
pop ebx			
pop ebp
ret
_oblicz_pi ENDP
END