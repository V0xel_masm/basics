.686
.model flat

public _suma_cyfr

.code
_suma_cyfr PROC
push ebp
mov ebp,esp
push ebx
mov eax, [ebp+8];wrzucamy liczbe
test eax, eax
jns nie_ujemna
neg eax
nie_ujemna:
mov ecx, 10 ;dzielnik
mov ebx, 0 ;suma cyfr
cmp eax, 0 ;sprawdzamy czy 0
je koniec ;gdy zero
dziel:
mov edx, 0 ;zerujemy starsza czesc dzielnej-robimy 64bitowa
div ecx ;dzielimy przez 10, glowna w eax, reszta w edx
add ebx, edx ;dodajemy reszte
cmp eax, 0
je koniec
jmp dziel
koniec:
mov eax, ebx
pop ebx
pop ebp
ret
_suma_cyfr ENDP
END