.686
.model flat

public _liczba_sekund

.code
_liczba_sekund PROC
push ebp
mov ebp,esp
push ebx
mov ebx, [ebp+16] ;wrzucamy liczbe sek
mov eax, [ebp+12] ;wrzucamy liczbe minut
mov ecx, 60 ;mnoznik dla minut
imul ecx ;mnozymy - wynik w eax
add ebx, eax ; dodajemy do glownego
mov eax, [ebp+8] ;dodajemy godziny
mov ecx, 3600 ;mnoznik dla godzin
imul ecx
add eax, ebx ;ostateczny wynik
pop ebx
pop ebp
ret
_liczba_sekund ENDP
END