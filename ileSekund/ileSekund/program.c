#include <stdio.h>
unsigned int liczba_sekund(unsigned int godziny, unsigned int minuty,
												unsigned int sekundy);

int main()
{
	unsigned int godz, min, sek, wynik;
	printf("\nProsze podac aktualny czas");
	printf(" w postaci liczb: ");
	scanf_s("%u %u %u", &godz, &min, &sek);
	wynik = liczba_sekund(godz, min, sek);
	printf("\nod poczatku doby \
uplynelo %u sekund\n", wynik);
	return 0;
}