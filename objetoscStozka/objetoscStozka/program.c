#include <stdio.h>
#include "conio.h"

float objetosc_stozka(float r, float h);

int main()
{

	float p, r, h;

	printf("\n Prosze podac promien podstawy i wysokosc stozka: ");
	scanf_s("%f %f", &r, &h);		// 20 -> max ilosc znakow (wlacznie z enterem)
	p = objetosc_stozka(r, h);
	printf("\n Objetosc stozka o r = %f i wysokosci = %f wynosi: %f \n", r, h, p);

	return 0;
}