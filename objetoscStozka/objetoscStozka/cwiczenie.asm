.686
.model flat

public _objetosc_stozka

.code
_objetosc_stozka PROC
push ebp
mov ebp,esp
push dword PTR 3
finit
fild dword PTR [esp] ;ladujemy 3 
add esp, 4
fld1 ;ladujemy 1, st0--1, st1--3
fdivrp st(1), st(0) ;dzielimy st0 przez st1 i wynik st1 i pop , w st0 1/3
fldpi ;ladujemy pi
fmulp st(1), st(0) ;mnozenie st1 z st0 i pop, w st0 pi*(1/3)
fld dword PTR [ebp+8] ;ladujemy r
fmul st(0), st(0)
fmulp st(1), st(0)
fld dword PTR [ebp+12] ;ladujemy h
fmulp st(1), st(0)
pop ebp
ret
_objetosc_stozka ENDP
END