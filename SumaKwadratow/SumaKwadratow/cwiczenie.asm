.686
.model flat

public _suma_kwadratow

.code
_suma_kwadratow PROC
push ebp
mov ebp,esp
push ebx
mov eax, [ebp+8] ;wrzucamy pierwsza
imul eax ;b^2, wynik edx:eax
mov ebx, eax  ;zapisujemy se do ebx
mov eax, [ebp+12];wrzucamy a
imul eax ;a^2
add eax, ebx ;a^2 + b^2
pop ebx
pop ebp
ret
_suma_kwadratow ENDP
END