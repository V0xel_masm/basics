.686
.model flat

public _obj_kuli

.code
_obj_kuli PROC ;wyznacza pole kola
push ebp
mov ebp,esp
push DWORD PTR 4
fild DWORD PTR[esp] ;to zamieni tez na zmiennoprzecinkowa
add esp, 4 ;usuniecie zawartosci wierzcholka zwyklego stosu
push DWORD PTR 3
fild DWORD PTR [esp]
add esp, 4
fdivp st(1), st(0) ;wynik w st(0) bo z "p"
fld DWORD PTR[ebp+8] ;;wrzucamy argument, to co w st(0) przesunie sie na st(1)
fst st(2)  ;przerzucamy z st(0) do st(2)
fmul st(0), st(0) ;R^2
fmul st(0), st(2) ;R^3
fmul st(0), st(1) ;razy 4/3 R^3
fldpi ;wrzucamy pi na st(0) reszta sie przesunie
fmul st(0), st(1)
pop ebp
ret
_obj_kuli ENDP
END