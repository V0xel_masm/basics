#include <stdio.h>
float pole_prostokata(float * pa, float * pb);
int main()
{
	float bok_a, bok_b, pole;
	printf("\nProsz poda boki prostokta: ");
	scanf_s("%f %f", &bok_a, &bok_b);
	pole = pole_prostokata(&bok_a, &bok_b);
	printf("\nPole prostokta wynosi %f\n", pole);
	return 0;
}