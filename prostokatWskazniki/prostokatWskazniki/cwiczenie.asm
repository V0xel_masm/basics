.686
.model flat

public _pole_prostokata

.code
_pole_prostokata PROC
push ebp
mov ebp,esp
mov eax, [ebp+8] ; adres pa do eax
mov ecx, [ebp+12] ;adres pb do eax

finit
fld dword PTR [ecx]
fld dword PTR [eax] ;liczba z eax bedzie w st(0)
fmul st(0), st(1)
pop ebp
ret
_pole_prostokata ENDP
END