.686
.model flat

public _suma_odwrotnosci

.code
_suma_odwrotnosci PROC
push ebp
mov ebp,esp
push ebx
finit
mov ebx, [ebp+8] ;adres tablicy w ebx 
fld dword PTR [ebp+12] ;rozmiar tablicy float laduje na st0
push dword PTR 0	;tu laduje wynik ;alokuje obszar na esp taki trick
fisttp dword PTR [esp] ;konwersja na int i zaladowanie do [esp] tego obszaru co wyzej
mov ecx, dword PTR [esp]	;laduje przkonwertowany wynik do ecx
add esp, 4
fldz ;zaladowanie 0 na koprocesor - to bedzie suma calkowita
fld1 ;zaladowanie 1
poczatek:
fld dword PTR [ebx] ;zaladowanie elementu st0, st1--1, st2--suma
fdivr st(0), st(1) ;st1 przez st0 i wynik w st0
faddp st(2), st(0) ;dodanie do sumy  1/r i pop wierzcholka w st0--1 a w st1--suma
add ebx, 4 ;przsuniecie w tablicy
loop poczatek
;fstp st(0) ;usuniecie wierzchola bo suma jest w st1 ;gdy chcemy sume odwrotnosci
fdiv st(0), st(1) ;gdy chcemy rezystancje zastepcza
pop ebx
pop ebp
ret
_suma_odwrotnosci ENDP
END