.686
.model flat

public _srednia

.code
_srednia PROC
push ebp
mov ebp,esp
push ebx
finit
fld dword PTR[ebp+12] ;rozmiar tablicy float
push dword PTR 0	;tu laduje wynik ;alokuje obszar na esp taki trick
fisttp dword PTR [esp] ;konwersja na int z ucieciem i zaladowanie do [esp] tego obszaru co wyzej
fild dword PTR [esp] ;zaladowanie scietego rozmiaru tablicy
mov ecx, dword PTR [esp]	;laduje wynik do ecx
add esp,4
mov ebx, [ebp+8] ;adres tabl do ebx
fldz ;ladujemy 0 --wynik ostateczny
dodawanie:
fld dword PTR [ebx]
faddp st(1), st(0)
add ebx, 4 ;przejscie na kolejny
loop dodawanie
fdiv st(0), st(1) ;podzielenie calosci przez ilosc elementow
pop ebx			
pop ebp
ret
_srednia ENDP
END